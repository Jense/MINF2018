﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastARCamera : MonoBehaviour {

    private RaycastHit hit;
    private bool hitObject;

    public GameObject obj;

    private GameObject spinning;
    private SpinningCubes spinningCubes;

    // Use this for initialization
    void Start () {
        spinning = GameObject.Find("Spinning");
        spinningCubes = spinning.GetComponent<SpinningCubes>();
        spinning.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        Transform cam = Camera.main.transform;
        Ray ray = new Ray(Camera.main.transform.position, cam.forward);
        Debug.DrawRay(ray.origin, ray.direction * 10000, Color.green, 10, false);

        if (Physics.Raycast(ray, out hit, 1000))
        {
                Debug.Log("Looking for " + obj.name + " hitting " + hit.transform.name);
                if (hit.transform.name == obj.name)
                {
                    Debug.Log("Found " + obj.name + "  :  " + hit.transform.name + " @ " + hit.transform.position);
                    hitObject = true;
                }
                else
                {
                    hitObject = false;
                }
        }

        if (hitObject)
        {
            ChangeMaterialColor(obj, new Color(0f, 1f, 0, 0.5f));
            spinning.SetActive(true);
            Vector3 pos = cam.position;

            float distance = pos.magnitude;
            spinningCubes.Speed = 1 / distance * 100.0f;
        }
        else
        {
            ChangeMaterialColor(obj, new Color(1f, 0, 0, 0.5f));
            spinning.SetActive(false);
            
            
        }
    }

    void ChangeMaterialColor(GameObject target, Color matColor)
    {
        Material materialColored;
        materialColored = new Material(Shader.Find("Transparent/Diffuse"));
        materialColored.color = matColor; //currentColor;
        target.GetComponent<Renderer>().material = materialColored;
    }
}

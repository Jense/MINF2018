﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinningCubes : MonoBehaviour
{
    public float Speed { get; set; }
    // Use this for initialization
    void Start()
    {
        Speed = 20.0f;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up * Time.deltaTime * Speed);
    }
}

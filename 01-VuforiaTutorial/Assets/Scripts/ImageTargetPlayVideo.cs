﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.Video;
using UnityEngine.UI;

public class ImageTargetPlayVideo : MonoBehaviour, ITrackableEventHandler
{
#region PRIVATE_MEMBERS
    private TrackableBehaviour mTrackableBehaviour;
    private VideoPlayer videoPlayer;
    private bool isMarkerDetected = false;
    #endregion

    #region PUBLIC_MEMBERS
    public Button m_PlayButton;
#endregion

	#region PRIVATE_METHODS
    // Use this for initialization
    void Start () {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
        videoPlayer = GetComponentInChildren<VideoPlayer>();
        
		// setup delegates for handle some video events
        videoPlayer.started += HandleStartedEvent;
        videoPlayer.prepareCompleted += HandlePrepareCompleted;
	}


    // Update is called once per frame
    void Update () {
        if (isMarkerDetected && videoPlayer.isPrepared && !videoPlayer.isPlaying)
        {
            videoPlayer.Play();
        }
        else if (!isMarkerDetected && videoPlayer.isPlaying)
            videoPlayer.Stop();	
	}
	#endregion

    #region PUBLIC_METHODS
	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			// Play video when target is found
			if (!isMarkerDetected)
			{
				videoPlayer.Prepare();
				isMarkerDetected = true;
			}
		}
		else
		{
			//    // Stop video when target is lost
			isMarkerDetected = false;
		}
	}

    #endregion // PUBLIC_METHODS


    #region DELEGATES
    void HandleStartedEvent(VideoPlayer video)
    {
        Debug.Log("Started: " + video.clip.name);
    }

    void HandlePrepareCompleted(VideoPlayer video)
    {
        Debug.Log("Prepare Completed: " + video.clip.name);
    }

	#endregion
}

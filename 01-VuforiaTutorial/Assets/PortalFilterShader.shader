﻿/*
 * To be attached to the mesh of the Portal
 * Should set render queue to before geometry, however this will
 * Result in objects closer than the portal to still be rendererd
 */


Shader "Custom/PortalFilterShader"
{
	SubShader
	{
		// remove objects behind the portal
		ZWrite off

		// transparent
		ColorMask 0

		// Bidirectional behaviours
		Cull off

		Stencil {
			Ref 1

			// Set all pixel in the portal to 1
			Pass replace
		}

		Pass
		{
		}
	}
}
